package com.sample.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sample.factory.CarFactory;
import com.sample.model.Car;

@Controller
@RequestMapping("/temp")
public class HelloRestController {

	@Autowired
	CarFactory factory;
	
	@RequestMapping(value = "/rest/{name}", method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String, String> getRestMovie(@PathVariable String name, ModelMap model) {
		Car c = factory.getCar(name);	
		if(c == null){
			HashMap<String,String> map = new HashMap<>();
			map.put("status", "failure");
			map.put("message", "Please input a valid model name");
			map.put("name","default");
			map.put("price","00000");
			map.put("engine","0000 cc");
			return map;
		}
		return c.getFeature();

	}

}
