package com.sample.controller;

import java.util.HashMap;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

@Controller
@RequestMapping("/temp")
public class HelloRestConsumerController {
	@RequestMapping(value = "/consumer/{name}", method = RequestMethod.GET)
	public String getRestMovie(@PathVariable String name, ModelMap model) {
		RestTemplate temp = new RestTemplate();
		HashMap<String,String> feature = temp.getForObject("http://localhost:8080/SampleSpring/temp/rest/" + name, HashMap.class);
		model.addAttribute("feature", feature);
		return "list";
	}
}
