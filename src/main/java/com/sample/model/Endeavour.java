package com.sample.model;

import java.util.HashMap;

public class Endeavour implements Car{
	@Override
	public HashMap<String, String> getFeature() {
		// to avoid redundancy we can model all these properties to a bean and then provide a function that converts it to hashmap
		HashMap<String,String> map = new HashMap<>();
		map.put("status", "success");
		map.put("message", "");
		map.put("name",this.getClass().getSimpleName());
		map.put("price","33333");
		map.put("engine","3000 cc");
		return map;
	}
}
