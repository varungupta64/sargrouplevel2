package com.sample.factory;

import org.springframework.stereotype.Component;

import com.sample.model.Car;
import com.sample.model.Ecosport;
import com.sample.model.Endeavour;
import com.sample.model.Figo;
import com.sample.model.Mondeo;

@Component("factory")
public class CarFactory {
	public Car getCar(String model) {
		if(model.equalsIgnoreCase("ecosport")){
			return new Ecosport();
		}else if(model.equalsIgnoreCase("figo")){
			return new Figo();
		}else if(model.equalsIgnoreCase("endeavour")){
			return new Endeavour();
		}else if(model.equalsIgnoreCase("mondeo")){
			return new Mondeo();
		}else{
			return null;
		}
	}
}
